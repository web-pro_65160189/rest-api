import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperetureService } from './tempereture.service';

@Controller('tempereture')
export class TemperetureController {
  constructor(private readonly temperetureService: TemperetureService) {}
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.temperetureService.convert(parseFloat(celsius));
  }
  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return this.temperetureService.convert(parseFloat(celsius));
  }
  @Post('convert')
  convertBypost(@Body('celsius') celsius: number) {
    return this.temperetureService.convert(celsius);
  }
}