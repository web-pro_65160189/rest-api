import { Module } from '@nestjs/common';
import { TemperetureController } from './tempereture.controller';
import { TemperetureService } from './tempereture.service';


@Module({
    imports: [],
    exports: [],
    controllers: [TemperetureController],
    providers: [TemperetureService],
  
})
export class TemperetureModule {}
